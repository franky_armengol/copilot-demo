# Copilot demo

This repo tries to test copilot. In this demo we want to deploy a nginx service in AWS Fargate.

## Prerequisites

- [Aws-cli](https://docs.aws.amazon.com/cli/latest/userguide/install-bundle.html)
- [Copilot-ci](https://github.com/aws/copilot-cli)

## Usage

### Create the application

We are creating a new application called demo with a Load Balancer and a ECS Service called nginx.
This command also creates for us: VPC and ECS Cluster. More [info](https://github.com/aws/copilot-cli/wiki/init-command)

```bash
copilot init --app demo                  \
  --svc nginx                            \
  --svc-type 'Load Balanced Web Service' \
  --dockerfile './Dockerfile'            \
  --deploy
```

### Update the service

If we want to update the service: More [info](https://github.com/aws/copilot-cli/wiki/svc-deploy-command)

```bash
copilot svc deploy -n nginx
```

### Delete the application

If we want to delete all the infrastructure created by copilot: More [info](https://github.com/aws/copilot-cli/wiki/app-delete-command)

```bash
copilot app delete demo
```


## Authors

Project maintained by [Francesc Armengol](https://gitlab.com/franky_armengol)
